<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//AUDITING
use OwenIt\Auditing\Auditable as AuditingAuditable;
use OwenIt\Auditing\Contracts\Auditable;


class db_agent_has_user extends Model implements Auditable
{
    protected $table = 'agent_has_client';
    public $timestamps = false;
    use AuditingAuditable;
}
